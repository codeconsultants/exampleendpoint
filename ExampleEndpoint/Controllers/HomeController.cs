﻿using ExampleEndpoint.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExampleEndpoint.Controllers
{
    /// <summary>
    /// The (standard MVC) Home Controller
    /// </summary>
    /// <seealso cref="Controller" />
    public class HomeController : Controller
    {
        /// <summary>
        /// Displays the home index view with the specified example endpoint settings
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="welcomeMessage">The welcome message.</param>
        /// <param name="additionalContent">Optional additional content.</param>
        /// <param name="hideCopyright">if set to <c>true</c> it should hide the copyright message.</param>
        /// <returns>An MVC view result</returns>
        [HttpGet]
        public IActionResult Index(string title = null, string welcomeMessage = null, string additionalContent = null, bool hideCopyright = false)
            => Index(new ExampleSettings(title, welcomeMessage, additionalContent, hideCopyright));

        /// <summary>
        /// Displays the home index view with the specified example endpoint settings
        /// </summary>
        /// <param name="exampleSettings">The example endpoint settings.</param>
        /// <returns>An MVC view result to display the home index view</returns>
        [HttpPost]
        public IActionResult Index(ExampleSettings exampleSettings)
            => View(exampleSettings);
    }
}
