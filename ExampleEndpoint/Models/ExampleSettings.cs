﻿namespace ExampleEndpoint.Models
{
    /// <summary>
    /// A model to capture example site settings
    /// </summary>
    public class ExampleSettings
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the welcome message.
        /// </summary>
        /// <value>
        /// The welcome message.
        /// </value>
        public string WelcomeMessage { get; set; }
        /// <summary>
        /// Gets or sets the additional content
        /// </summary>
        /// <value>
        /// The additional content.
        /// </value>
        public string AdditionalContent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to hide the copyright message.
        /// </summary>
        /// <value>
        ///   <c>true</c>, if it should hide the copyright message, otherwise <c>false</c>.
        /// </value>
        public bool HideCopyright { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExampleSettings"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="welcomeMessage">The welcome message.</param>
        /// <param name="additionalContent">The additional content.</param>
        /// <param name="hideCopyright"></param>
        public ExampleSettings(string title = null, string welcomeMessage = null, string additionalContent = null, bool hideCopyright = false)
        {
            Title = string.IsNullOrEmpty(title) ? "Example Endpoint" : title;
            WelcomeMessage = string.IsNullOrEmpty(welcomeMessage) ? "Welcome to the example endpoint site.  This just hosts a simple page which can be customised to display a welcome message, a different title and additional content to be used when mocking up interfaces linking to external sites." : welcomeMessage;
            AdditionalContent = string.IsNullOrEmpty(additionalContent) ? null : additionalContent;
            HideCopyright = hideCopyright;
        }
    }
}
